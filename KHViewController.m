//
//  KHViewController.m
//  KeHayDeComer
//
//  Created by Rflpz on 16/08/14.
//  Copyright (c) 2014 Rflpz. All rights reserved.
//

#import "KHViewController.h"
#import "KHClientViewController.h"
#import "KHCustomCell.h"

static NSString *urlBase = @"http://kehaydecomer.com/sow/index.php/apis/clientes";

@interface KHViewController ()<UITableViewDelegate, UITableViewDataSource>
@property(weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) NSArray *arrayData;
@property (strong, nonatomic) NSIndexPath *lastSelectedIndexPath;
@end

@implementation KHViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    [self customizeViewInterface];
}

#pragma  mark - Create to refreshControl and add subview to table view
- (void)customizeViewInterface {
    UIRefreshControl *refreshControl= [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self
                       action:@selector(refreshInfo:)
             forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    self.refreshControl =refreshControl;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.title = @"Ke hay de comer";
    [self.refreshControl beginRefreshing];
    [self refreshInfo:self.refreshControl];
}

#pragma mark - Download the JSON and save in NSDictironary
-(void)refreshInfo:(UIRefreshControl *)sender{
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:urlBase] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!error){
            
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"%@",json);
            self.arrayData = [json valueForKeyPath:@"clientes"];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self.tableView reloadData];
                [sender endRefreshing];
            }];
        }
        else{
            NSLog(@"Hubo un error");
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [sender endRefreshing];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No fue posible acceder a internet, verifica tu conexión" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }];
        }
        
    }];
    [dataTask resume];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrayData.count;
}

-(UITableViewCell *)tableView:(UITableViewCell *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"KHCustomCell";
    
     KHCustomCell *cell = (KHCustomCell *)[self.tableView dequeueReusableCellWithIdentifier:cellId];
    
    //UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"KHCustomCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    NSDictionary *data = self.arrayData[indexPath.row];
    
   /* NSString *infoImageURL = [data valueForKey:@"fotografia"];
    NSURL *NSImage = [NSURL URLWithString:infoImageURL];
    UIImage *imgClient = [UIImage imageWithData:[NSData dataWithContentsOfURL:NSImage]];
    cell.clientImg.image = imgClient;*/
    UIImage *img = [UIImage imageNamed:@"prueba.png"];
    cell.clientImg.image = img;
    cell.clientName.text = [data valueForKey:@"nombre"];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idClientIdentifier = self.arrayData[indexPath.row][@"id"];
    NSString *nameClient = self.arrayData[indexPath.row][@"nombre"];
    KHClientViewController *clientController = [[KHClientViewController alloc]initWithNibName:@"KHClientViewController"bundle:nil];
    self.lastSelectedIndexPath = indexPath;
    clientController.nameClient = nameClient;
    clientController.idClient = idClientIdentifier;
    [self.navigationController pushViewController:clientController animated:YES];
    [self.tableView deselectRowAtIndexPath:self.lastSelectedIndexPath animated:YES];
}
@end
