//
//  KHAppDelegate.h
//  KeHayDeComer
//
//  Created by Rflpz on 16/08/14.
//  Copyright (c) 2014 Rflpz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
