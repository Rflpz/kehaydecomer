//
//  KHCustomCell.m
//  KeHayDeComer
//
//  Created by Rflpz on 23/08/14.
//  Copyright (c) 2014 Rflpz. All rights reserved.
//

#import "KHCustomCell.h"

@implementation KHCustomCell
@synthesize clientImg = _clientImg;
@synthesize clientName = _clientName;

- (void)awakeFromNib
{
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
