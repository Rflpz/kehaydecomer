//
//  main.m
//  KeHayDeComer
//
//  Created by Rflpz on 16/08/14.
//  Copyright (c) 2014 Rflpz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KHAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KHAppDelegate class]));
    }
}
