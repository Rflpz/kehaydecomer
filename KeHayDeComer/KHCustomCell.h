//
//  KHCustomCell.h
//  KeHayDeComer
//
//  Created by Rflpz on 23/08/14.
//  Copyright (c) 2014 Rflpz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KHCustomCell : UITableViewCell
@property (weak,nonatomic) IBOutlet UIImageView *clientImg;
@property (weak,nonatomic) IBOutlet UILabel *clientName;
@end
