//
//  KHClientViewController.h
//  KeHayDeComer
//
//  Created by Rflpz on 16/08/14.
//  Copyright (c) 2014 Rflpz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KHClientViewController : UIViewController
    @property (strong,nonatomic) NSString *idClient;
    @property (strong, nonatomic) NSString *nameClient;
@end
