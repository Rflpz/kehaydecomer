//
//  KHClientViewController.m
//  KeHayDeComer
//
//  Created by Rflpz on 16/08/14.
//  Copyright (c) 2014 Rflpz. All rights reserved.
//

#import "KHClientViewController.h"
@interface KHClientViewController ()
@property (strong, nonatomic) NSDictionary *infoClient;
@property (weak, nonatomic) NSString *urlBase;
@end

@implementation KHClientViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.urlBase =@"http://kehaydecomer.com/sow/index.php/apis/clientes?id=";
    self.title = self.nameClient;
    self.urlBase = [NSString stringWithFormat:@"%@%@",self.urlBase,self.idClient];
    NSLog(@"%@",self.urlBase);
    [self downlaodInfo];
}
-(void)downlaodInfo{
    NSURLSession *sesion = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [sesion dataTaskWithURL:[NSURL URLWithString:self.urlBase] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"%@",json);
        self.infoClient = [json valueForKeyPath:@"clientes"];
        if (!error){
            [[NSOperationQueue mainQueue] addOperationWithBlock:^(void)
             {
                 //TODO
                 NSString *nombre = [self.infoClient valueForKey:@"nombre"];
                 NSLog(@"%@",nombre);
             }];
        }
        else{
            NSLog(@"Hubo un error");
            [[NSOperationQueue mainQueue] addOperationWithBlock:^(void)
             {
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"No fue posible acceder a internet, verifica tu conexión" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 [alert show];
             }];
        }
        
    }];
    [dataTask resume];
}
@end
